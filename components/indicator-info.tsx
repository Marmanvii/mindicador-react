import React, { useEffect, useState } from 'react';
import 'fontsource-roboto';
import Moment from 'moment';
import { Card, CardContent, makeStyles, Typography } from '@material-ui/core';
import { LineChart, Grid, YAxis } from 'react-native-svg-charts';

interface Indicador {
    version: string;
    autor: string;
    codigo: string;
    nombre: string;
    unidad_medida: string;
    serie: Array<{
        fecha: string;
        valor: string;
    }>
}

const useStyles = makeStyles({
    root: {
        minWidth: 275,
        margin: 10
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
});



const IndicatorInfo = ({ route, navigation }: any) => {

    const classes = useStyles();
    const [indicator, setIndicator] = useState<Indicador>();
    const { code } = route.params;
    const bull = <span className={classes.bullet}>•</span>;

    //Chart
    const data = [50, 10, 40, 95, -4, -24, 85, 91, 35, 53, -53, 24, 50, -20, -80];
    const contentInset = { top: 20, bottom: 20 }

    useEffect(() => {
        fetch(`https://mindicador.cl/api/${code}`)
            .then((response) => response.json())
            .then((json) => {
                setIndicator(json);
            })
            .catch((error) => {
                console.error(error);
            })
    }, []);

    return (
        <>
            <Card className={classes.root} variant="outlined">
                <CardContent>
                    <Typography className={classes.title} color="textSecondary" gutterBottom>
                        {indicator?.nombre}
                    </Typography>
                    <Typography variant="body1">
                        <b>{bull} Código: </b>{indicator?.codigo}
                    </Typography>
                    <Typography variant="body1">
                        <b>{bull} Unidad de Medida: </b>{indicator?.unidad_medida}
                    </Typography>
                    <Typography variant="body1">
                        <b>{bull} Versión: </b>{indicator?.version}
                    </Typography>
                </CardContent>
            </Card>
            <Card className={classes.root} variant="outlined">
                <YAxis
                    data={data}
                    contentInset={contentInset}
                    svg={{
                        fill: 'grey',
                        fontSize: 10,
                    }}
                    numberOfTicks={10}
                    formatLabel={(value) => `${value}ºC`}
                />
                <LineChart
                    style={{ flex: 1, marginLeft: 16 }}
                    data={data}
                    svg={{ stroke: 'rgb(134, 65, 244)' }}
                    contentInset={contentInset}
                >
                    <Grid />
                </LineChart>
            </Card>
        </>
    );
}

export default IndicatorInfo;