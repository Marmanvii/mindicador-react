import { Card, CardContent, Divider, List, ListItem, ListItemText, makeStyles, Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import Moment from 'moment';

interface Indicador {
    version: string;
    autor: string;
    codigo: string;
    nombre: string;
    unidad_medida: string;
    serie: Array<{
        fecha: string;
        valor: string;
    }>
}

const useStyles = makeStyles({
    root: {
        minWidth: 275,
        margin: 10
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
});

export default function IndicatorHistory({ route, navigation }: any) {

    const classes = useStyles();

    const { code } = route.params;

    const [indicator, setIndicator] = useState<Indicador>();

    useEffect(() => {
        fetch(`https://mindicador.cl/api/${code}`)
            .then((response) => response.json())
            .then((json) => {
                setIndicator(json);
            })
            .catch((error) => {
                console.error(error);
            })
    }, []);

    return (
        <Card className={classes.root} variant="outlined">
            <CardContent>
                <Typography variant="h5" gutterBottom>
                    Historial de Valores
                </Typography>
                <Divider />
                <List component="nav" aria-label="secondary mailbox folders">
                    {indicator?.serie.map((item, index) => (
                        <ListItem key={`${index}_${item}`} button divider>
                            <ListItemText primary={item.valor} secondary={Moment(item.fecha, 'YYYY-MM-DDTHH:mm').format('DD-MM-YYYY HH:mm')} />
                        </ListItem>
                    ))}
                </List>
            </CardContent>
        </Card>
    );

}