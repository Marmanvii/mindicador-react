import { StatusBar } from 'expo-status-bar';
import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import 'fontsource-roboto';
import { Button, Divider, FormControl, IconButton, List, ListItem, ListItemIcon, ListItemProps, ListItemSecondaryAction, ListItemText, TextField } from '@material-ui/core';
import InfoOutlined from '@material-ui/icons/InfoOutlined';

interface Mindicador {
    version: string;
    autor: string;
    fecha: string;
    uf: Indicador;
    ivp: Indicador;
    dolar: Indicador;
    dolar_intercambio: Indicador;
    euro: Indicador;
    ipc: Indicador;
    utm: Indicador;
    imacec: Indicador;
    tpm: Indicador;
    libra_cobre: Indicador;
    tasa_desempleo: Indicador;
    bitcoin: Indicador;
}

interface Indicador {
    codigo: string;
    nombre: string;
    unidad_medida: string;
    fecha: string;
    valor: number;
}

export default function IndicatorList({ navigation }: any) {
    const notIndicators: Array<string> = ['version', 'autor', 'fecha'];
    const [list, setList] = useState<Array<Indicador>>([]);

    function mapMindicadorToArrayIndicador(mindicador: Mindicador): Array<Indicador> {
        const indicators: Array<Indicador> = new Array<Indicador>();

        Object.values(mindicador).forEach(value => {
            if (typeof (value) !== 'string') {
                indicators.push(value as Indicador);
            }
        });

        return indicators;
    }

    useEffect(() => {
        fetch('https://mindicador.cl/api')
            .then((response) => response.json())
            .then((json) => {
                setList(mapMindicadorToArrayIndicador(json));
            })
            .catch((error) => {
                console.error(error);
            });
    }, []);

    const viewHistory = (code: string) => {
        navigation.navigate('Historial', {
            code
        })
    }

    const viewInfo = (code: string) => {
        navigation.navigate('Información', {
            code
        })
    }

    return (
        <>
            <List component="nav" aria-label="secondary mailbox folders">
                {list.map((item, index) => (
                    <ListItem key={`${index}_${item}`} button onClick={() => viewHistory(item.codigo)} divider>
                        <ListItemText primary={item.nombre} secondary={item.unidad_medida} />
                        <ListItemSecondaryAction>
                            <IconButton edge="end" aria-label="comments" onClick={() => { viewInfo(item.codigo) }}>
                                <InfoOutlined />
                            </IconButton>
                        </ListItemSecondaryAction>
                    </ListItem>
                ))}
            </List>
            {/* <Button variant="outlined" color="primary" onClick={goToDetail}>Agregar</Button> */}
        </>
    );
}

