import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import DenseAppBar from './components/app-bar';
import IndicatorList from './components/indicator-list';
import IndicatorInfo from './components/indicator-info';
import IndicatorHistory from './components/indicator-history';

const Stack = createStackNavigator();

export default function App() {
  return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Lista de Indicadores">
          <Stack.Screen name="Lista de Indicadores" component={IndicatorList} />
          <Stack.Screen name="Información" component={IndicatorInfo} />
          <Stack.Screen name="Historial" component={IndicatorHistory} />
        </Stack.Navigator>
      </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
